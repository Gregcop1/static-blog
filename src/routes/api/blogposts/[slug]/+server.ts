import {error, json} from '@sveltejs/kit';
import type {RequestEvent} from '@sveltejs/kit';
import {fetchBlogPost} from '$lib/server/helpers/fetcher';

export const GET = async ({ params }: RequestEvent): Promise<Response> => {
    const blogPost = await fetchBlogPost(params.slug!);

    if (blogPost === null) {
        throw error(404, 'Unknown blog post.');
    }

    return json(blogPost);
}
