import {json} from '@sveltejs/kit';
import type {RequestEvent} from '@sveltejs/kit';
import {fetchBlogPosts} from '$lib/server/helpers/fetcher';
import type {BlogPostQuery} from '$lib/interfaces';

export const GET = async ({ url }: RequestEvent): Promise<Response> => {
    const params: BlogPostQuery = Object.fromEntries(url.searchParams);
    const blogPosts = await fetchBlogPosts(params);

    return json(blogPosts);
}
