import type {LoadEvent} from '@sveltejs/kit';
import type {BlogPost} from '$lib/interfaces';
import {error} from '@sveltejs/kit';

export const load = async ({ fetch, url }: LoadEvent) => {
    const response = await fetch('/api/blogposts');

    if (!response.ok) {
        throw error(response.status, response.statusText);
    }

    return {
        blogposts: await response.json() as BlogPost[],
    };
}
