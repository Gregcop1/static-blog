import {basename, extname} from 'path';
import type {BlogPost, BlogPostMetaData, BlogPostOrder, BlogPostQuery} from '$lib/interfaces';

interface BlogPostFile {
    metadata: BlogPostMetaData;
    default: {
        $$render: () => string;
    };
}

const extractBlogPostContent = async (path: string, content: string, metadata: BlogPostMetaData): Promise<BlogPost> => {
    const excerptIndex = content.indexOf('<more>');
    const excerpt = content.slice(0, excerptIndex)
        .replace(/<\/?[^>]+(>|$)/g, "");

    return {
        content,
        excerpt,
        metadata,
        path: basename(path)
            .replace(extname(path), ''),
    }
};

export const fetchBlogPostFiles = async (): Promise<BlogPost[]> => {
    const blogPostFiles = import.meta.glob('/src/posts/*.md');

    const iterablePostFiles = Object.entries(blogPostFiles);

    return await Promise.all(
        iterablePostFiles.map(async ([path, resolver]) => {
            const {default: content, metadata} = (await resolver()) as BlogPostFile;
            return extractBlogPostContent(
                path,
                content.$$render ? content.$$render() : '',
                metadata,
            );
        })
    );
}

export const getQueryOrder = (orderQuery: BlogPostOrder) => ({
    direction: orderQuery[0] === '-' ? -1 : 1,
    order: orderQuery[0] === '-' ? orderQuery.substring(1) : orderQuery,
});

export const fetchBlogPosts = async (query?: BlogPostQuery): Promise<BlogPost[]> => {
    let blogPosts = await fetchBlogPostFiles();

    if(query?.category) {
        blogPosts = blogPosts.filter(post => post.metadata.category === query.category);
    }

    if(query?.tag) {
        blogPosts = blogPosts.filter(post => post.metadata.tags?.includes(query.tag!));
    }

    if (!query?.order) {
        return blogPosts;
    }

    const {direction, order} = getQueryOrder(query.order);

    return blogPosts.sort((a, b) => {
        // @ts-ignore-next-line
        return a.metadata[order] > b.metadata[order] ? direction : (-1 * direction);
    });
}

export const fetchBlogPost = async (slug: string): Promise<BlogPost|null> => {
    try {
        const path = `/src/posts/${slug}.md`;
        const {default: content, metadata} = await import(/* @vite-ignore */ path);

        return extractBlogPostContent(
            path,
            content.$$render ? content.$$render() : '',
            metadata,
        );
    } catch (e) {
        return null;
    }
}
