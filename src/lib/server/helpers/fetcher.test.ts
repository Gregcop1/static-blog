import {describe, expect, it} from 'vitest';
import {fetchBlogPost, fetchBlogPosts, getQueryOrder} from './fetcher';

describe.concurrent('API blog posts - order', async () => {
    it('should get order', async  () => {
        const {direction, order} = getQueryOrder('title');

        expect(order).toBe('title');
        expect(direction).toBe(1);
    });

    it('should get reverse order', async  () => {
        const {direction, order} = getQueryOrder('-title');

        expect(order).toBe('title');
        expect(direction).toBe(-1);
    });
});

describe.concurrent('API blog posts - category', async () => {
    it('should allow filter by category', async  () => {
        const targetCategory = 'Category 1';
        const blogPosts = await fetchBlogPosts({ category: targetCategory });

        blogPosts.forEach(post => {
            expect(post.metadata.category).toBe(targetCategory);
        });
    });
});

describe.concurrent('API blog posts - tag', async () => {
    it('should allow filter by category', async  () => {
        const targetTag = 'Tag 1';
        const blogPosts = await fetchBlogPosts({ tag: targetTag });

        blogPosts.forEach(post => {
            expect(post.metadata.tags).toContain(targetTag);
        });
    });
});

describe.concurrent('API blog posts - collection', async () => {
    it('should find all blog posts', async () => {
       const blogPosts = await fetchBlogPosts();

        expect(blogPosts).toHaveLength(3);
    });

    it('should fill blog post metadata', async  () => {
        const blogPost = (await fetchBlogPosts()).shift()!;

        expect(blogPost.metadata.title).toBe('First post');
        expect(blogPost.path).toBe('first-post');
    });
});

describe.concurrent('API blog posts - single', async () => {
    it('should find blog post', async () => {
       const blogPost = await fetchBlogPost('first-post');

        expect(blogPost!.metadata.title).toBe('First post');
    });

    it('should reject wrong slug', async () => {
       const blogPost = await fetchBlogPost('wrong-post');

        expect(blogPost).toBeNull();
    });
});
