export interface BlogPostMetaData {
    title: string;
    publicationDate: string;
    backgroundColor?: string;
    category?: string;
    tags?: string[];
}

export interface BlogPost {
    content: string;
    excerpt: string;
    metadata: BlogPostMetaData;
    path: string;
}

export type BlogPostOrder = "title" | "-title" | "publicationDate" | "-publicationDate";

export interface BlogPostQuery {
    order?: BlogPostOrder;
    category?: string;
    tag?: string;
}
