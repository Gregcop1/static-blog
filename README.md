# Static blog

Let's create a simple static blog with blog posts as markdown.

## How to create my blog

- Clone this repo
- Update configuration
- Link your repo to vercel or the static server you want to use
- Write some blog post
- That's it!
